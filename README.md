# btlabz-ansible-umami-playbook

## Overview

This playbook manages umami development node.

See: https://umami.is/docs/install
See: https://github.com/mikecao/umami

- nginx configured as reverse proxy
- self-signed certificate ssl termination
- database on the same host
- umami as a systemd service
- limited idempotency and change management testing: db schema, service configs, enironment
- ansible vault to store secrets

## Requirements

- A debian 10 host. You can use [terraform-aws-btlabz-umami-dev-layer](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) to deploy one.
- Workstation with the fresh ansible

## Playbooks

- umami-dev.yaml - configure development umami workstation

## Roles

- common: basic packages and os configurations
- nodejs-init: nodejs system via PPA
- postgres-init: local postgres server
- umami-init: umami application and server
- nginx-init: nginx as umami reverse proxy

## Runbook

### Prerequisites

Make sure you have information ready:

- host external address
- private ssh key file
- vault password

### Node deploy

Configure ssh config file like this.

```env
Host ec2-*.eu-west-3.compute.amazonaws.com
  User admin
  IdentityFile ~/.ssh/roman-syrtsev-20220525-ghost.pem
```

Clone ansible playbook

```bash
git clone https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook.git
cd btlabz-ansible-umami-playbook
```

Update dev inventory. You can have one or many hosts.

```ini
[umami]
node_01 ansible_host=ec2-set-your-host-here.eu-west-3.compute.amazonaws.com
node_02 ansible_host=ec2-set-another-host-here.eu-west-3.compute.amazonaws.com
```

Run playbook

```bash
ansible-playbook umami-dev.yaml -i inventory-dev --ask-vault-pass
```

## TODOs

- run umami service as non-privileged user
- use pm2 for nodejs process management
- valid non-self-signed certs for nginx (acme)
- selinux profiles for umami
- separate and version roles
- resolve idempotency and ansible-lint issues
- dynamic ec2 invntory

## References

- [T1: apache log analisys with python, bash, sqlite and docker](https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc)
- [T2: terraform node](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) and [ansible playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)
- [T3: Umami HA and resilience considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-ha)
- [T4: Umami monitoring considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)
